*start

[cm]
[clearfix]
[chara_config pos_mode=false]

[mask time=0]


[position layer="message0" left=20 top=450 width=920 height=150 page=fore visible=true]

[position layer=message0 page=fore margint="35" marginl="50" marginr="70" marginb="60"]

[ptext name="chara_name_area" layer="message0" color="white" size=22 x=50 y=460]

[chara_config ptext="chara_name_area"]

@showmenubutton

[chara_new name="akane" storage="chara/akane/normal.png" jname="キャラ１"]
[chara_new name="aoi" storage="chara/aoi/normal.png" jname="キャラ２"]

[chara_face name="akane" face="happy" storage="chara/akane/happy.png" jname="キャラ１"]
[chara_face name="akane" face="normal" storage="chara/akane/normal.png" jname="キャラ１"]
[chara_face name="akane" face="angry" storage="chara/akane/angry.png" jname="キャラ１"]
[chara_face name="akane" face="doki" storage="chara/akane/doki.png" jname="キャラ１"]
[chara_face name="akane" face="sad" storage="chara/akane/sad.png" jname="キャラ１"]

[chara_face name="aoi" face="happy" storage="chara/aoi/happy.png" jname="キャラ２"]
[chara_face name="aoi" face="normal" storage="chara/aoi/normal.png" jname="キャラ２"]
[chara_face name="aoi" face="angry" storage="chara/aoi/angry.png" jname="キャラ２"]
[chara_face name="aoi" face="doki" storage="chara/aoi/doki.png" jname="キャラ２"]
[chara_face name="aoi" face="sad" storage="chara/aoi/sad.png" jname="キャラ２"]


;背景
[image layer=0 folder="bgimage" name="gaikan" storage="gaikan_hiru.jpg" visible=true top=0 left=0 width=1200 height=675]

[chara_mod  name="akane" face="normal"]
[chara_mod  name="aoi" face="sad"]
[chara_show  name="aoi" left=180 top=90 layer="0"]

[mask_off time=1000]
[wait time=200]

[anim name=aoi left="-=8" time=100]
[wait time=100]
[anim name=aoi left="+=8" time=100]
[wait time=100]
[anim name=aoi left="-=8" time=100]
[wait time=100]
[anim name=aoi left="+=8" time=100]
[wait time=100]
[anim name=aoi left="-=8" time=100]
[wait time=1000]
[chara_hide name=aoi" ]

[camera x=100 zoom=1.2 time=800] 

[chara_mod  name="akane" face="normal"]
[chara_show width="320" height="480" left=400 top=110 name="akane"]

[wait time=200]

@layopt layer=message0 visible=true

#キャラA　
……………………[l]

[cm]

@layopt layer=message0 visible=false 
[chara_mod  name="akane" face="happy"]
[anim name=akane left="-=50" width=300 time=200 opacity=255]
[wait time=200]
[chara_hide name=akane" ]
[wait time=300]

[camera x=0 zoom=1 time=800]

[chara_mod  name="akane" face="normal"]
[chara_mod  name="aoi" face="sad" ]
[chara_show  name="aoi" left=180 top=90 zindex="2" time=200]
[chara_show  name="akane"  width="400" height="600"  left=500 top=50 zindex="1"time=100]

[wa]

[anim name=akane left="-=50" time=200]
[wa]

[anim name=akane left="-=25" top="+=5" time=200]
[anim name=aoi left="-=5"  top="+=3" time=200]

[wa]

[anim name=akane left="+=15" top="-=5" time=200]
[anim name=aoi left="+=5"  top="-=3" time=200]

[wa]

[anim name=akane left="-=25" top="+=5" time=200]
[anim name=aoi left="-=5"  top="+=3 time=200]

@layopt layer=message0 visible=true


#キャラA　
……つんつん[l]

[cm]


[chara_mod  name="aoi" face="doki"]

[quake time=1000]
[anim name=aoi top="-=50" left="-=50" time=100]

[anim name=aoi top="+=12" time=100]

#キャラB　
[font size=40]ぎゃ～！！[l]

[cm]


[chara_mod  name="aoi" face="angry"]


#キャラB　
何するのよ！！！　[r]
足しびれてたのに～！！！！[l]

[cm]

[chara_mod  name="akane" face="happy"]


#キャラA　
あはは、ごめんごめん[l]

[cm]

[mask time=1000]
[chara_hide name="aoi" time="0"]
[chara_hide name="akane" time="0"]


[chara_mod  name="akane" face="normal"]
[chara_mod  name="aoi" face="normal"]
[chara_show  name="aoi"  left=80 top=50 time="0"]
[chara_show  name="akane" left=480 top=50 time="0"]


[mask_off time=1000]

[anim name=akane top="-=10" time=100]
[wait time=100]
[anim name=akane top="+=10" time=100]
[wait time=100]
[anim name=akane top="-=10" time=100]
[wait time=100]
[anim name=akane top="+=10" time=100]
[wait time=200]

#キャラA　
ねえねえ、これは何？[l]

[cm]

[chara_mod  name="aoi" face="normal"]

[anim name=aoi top="-=10" time=100]
[wait time=100]
[anim name=aoi top="+=10" time=100]

#キャラB　
ああ、それね。焼き芋よ[l]

[cm]

[chara_mod  name="akane" face="happy"]

[anim name=akane left="-=10" time=200]
[wait time=100]
[anim name=akane left="+=10" time=200]

#キャラA　
へ～、おいしそうだね！[l]

[cm]

[chara_mod  name="aoi" face="happy"]

#キャラB　
今、焼くから待ってなさい[l]

[cm]

[mask time=1000]
[chara_hide name="aoi" time=0]
[chara_mod name="akane" face="sad" left=380]
@layopt layer=message0 visible=false

[mask_off time=1000]
[chara_mod name="akane" face="sad" reflect="true" ]
[anim name="akane" time=600 left="+=100" ]
[wa]

[chara_mod name="akane" face="sad" reflect="false"]
[anim name="akane" time=600 left="-=100" ]
[wa]

[chara_mod name="akane" face="sad" reflect="true"]
[anim name="akane" time=600 left="+=100"]

[wa]

[chara_mod name="akane" face="sad" reflect="false"]
[anim name="akane" time=600 left="-=100"]
[wa]
@layopt layer=message0 visible=true
#キャラA　
……

[cm]
[chara_mod  name="aoi" face="normal"]
[chara_show  name="aoi" left=80 top=50 layer="0"]
[wa]

#キャラB　
落ち着かないみたいだけど、どうしたの？[l]

[cm]

[chara_mod  name="akane" face="normal"]

#キャラA　
そろそろ焼けたかなって……[l]

[cm]

[anim name=aoi top="-=10" time=100]
[wait time=100]
[anim name=aoi top="+=10" time=100]

#キャラB　
そうね、もう少し時間が掛かると思うわ[l]

[cm]

[chara_mod  name="akane" face="sad"]

[anim name=akane top="-=10" time=100]
[wait time=100]
[anim name=akane top="+=10" time=100]


#キャラA　
分かった……[l]

[cm]

[mask time=1000]

[chara_hide name="akane" time="0"]
[mask_off time=1000]


[anim name=aoi top="-=10" time=100]
[wait time=100]
[anim name=aoi top="+=10" time=100]

[wait time=300]
[anim name=aoi left="+=20" effect="easeInOutQuint" time=300]

[chara_show  name="akane" face="happy" left=480 top=50 layer="0"]


[wait time=500]


#キャラB　
うん、いい感じ！　[r]
はい、どうぞ[l]


[cm]

[chara_mod  name="aoi" face="happy"]

#キャラB　
それじゃあ[l]

[cm]

[chara_hide name="akane" time=0]
[chara_hide name="aoi" time=0]

[image layer="0" folder="bgimage" name="gaikan" storage="gaikan_hiru.jpg" visible="true" top="-50" left="0" width="1200" height="675"]
[image layer="1" folder="bgimage" name="kuro" storage="kuro.jpg" visible="true" top="450" left="0" time="100" wait=false]
[image layer="1" folder="bgimage" name="kuro" storage="kuro.jpg" visible="true" top="-1020" left="0" time="100" wait=true]

[chara_show name="aoi" face="happy" left=-120 top=-50 width="800" height="1200" wait=false]
[chara_show name="akane" face="happy" left=300 top=-50 width="800" height="1200" wait=true]

[wait time=1000]

@layopt layer=message0 visible=true


#キャラA・B
「「いっただきまーす！」」[l]

[cm]

[chara_mod  name="akane" face="doki"]
[chara_mod  name="aoi" face="normal"]

[wait time=500]

[freeimage layer=1]
[camera x=230 zoom=2 time=500] 

[mask color=0xF0101 time=20]
@layopt layer=message0 visible=false
[chara_hide name="akane" time=0]
[chara_hide name="aoi" time=0]
[camera x=0 y=0 zoom=1 time=0]
[mask_off time=100]

[wait time=200]

[mask color=0xF01010 time=60]
[mask_off time=60]

[wait time=300]

@layopt layer=message0 visible=true

[quake time=600 vmax=0 hmax=10 count=8]
[font color=red size=40]
#キャラA　
……う、うぅ～！！！！！[l]

[cm]

[chara_mod name="aoi" face="doki" time=0]
[chara_show name="aoi" left=80  top=-80  width="800" height="1200" ]

[anim name=aoi top="-=10" time=100]
[wait time=100]
[anim name=aoi top="+=10" time=100]


#キャラB　
……熱いって！？　[r]
大変、お水お水！[l]

[mask time=500]
@layopt layer=message0 visible=false
@hidemenubutton
[chara_hide_all]
[freeimage layer="base"]
[mask_off]
[jump first.ks]

