
;■条件
;・キャラクターはあかね、やまとを使用
;・背景は何を使用してもOK

;■スクリプトを組む上でのポイント
;・鏡を見ている際にどのような演出をするか

*start

[cm]
[clearfix]
[chara_config pos_mode=false]

[position layer="message0" left=20 top=400 width=920 height=200 page=fore visible=true]

[position layer=message0 page=fore margint="45" marginl="50" marginr="70" marginb="60"]

[ptext name="chara_name_area" layer="message0" color="white" size=24 x=50 y=410]

[chara_config ptext="chara_name_area"]

@layopt layer=message0 visible=true

;窶披披披披披披披蝿ﾈ下よりスクリプトを組んでください窶披披披披披披披髏

[mask]

;キャラ１定義
[chara_new name="akane" storage="chara/akane/normal.png" jname="キャラ１"]
;キャラ２定義
[chara_new name="yamato" storage="chara/yamato/normal.png" jname="キャラ２"]
;キャラ１表情
[chara_face name="akane" face="normal" storage="chara/akane/normal.png" jname="キャラ１"]
[chara_face name="akane" face="happy" storage="chara/akane/happy.png" jname="キャラ１"]
[chara_face name="akane" face="doki" storage="chara/akane/doki.png" jname="キャラ１"]
[chara_face name="akane" face="sad" storage="chara/akane/sad.png" jname="キャラ１"]
[chara_face name="akane" face="angry" storage="chara/akane/angry.png" jname="キャラ１"]
[chara_face name="akane" face="back" storage="chara/akane/back.png" jname="キャラ１"]

;キャラ２表情
[chara_face name="yamato" face="happy" storage="chara/yamato/happy.png" jname="キャラ２"]
[chara_face name="yamato" face="angry" storage="chara/yamato/angry.png" jname="キャラ２"]
[chara_face name="yamato" face="tohoho" storage="chara/yamato/tohoho.png" jname="キャラ２"]
[chara_face name="yamato" face="normal" storage="chara/yamato/back.png" jname="キャラ２"]


;背景
[camera x=0 y=-40 zoom=0.6  time="100"]

[bg storage="51d2c967e2e8ca39628a-L.jpg"  time="100"]
;メニューボタン
@showmenubutton

[chara_show name="akane" left=50 top=0  width="800" height="1200"  time="100"]
[chara_mod name="akane"  face="normal"]
[playbgm storage="sad.ogg"]
[mask_off]

[anim name=akane left="-=20" time=400]
[wait time=400]
[anim name=akane left="+=10" time=400]
[wait time=400]
[anim name=akane left="-=20" time=400]
[wait time=400]
[anim name=akane left="+=20" time=400]
[wait time=400]
[wa]

#キャラ１ 
鏡よ、鏡よ、鏡さん[l]




[cm]

[chara_mod name="akane"  face="happy"]

[wait time=100]
[anim name=akane left="-=30" top="+=30" time=400 effect="easeOutCirc"]

#キャラ１ 
世界で一番美しいのはだーれ？[l]

[cm]

[mask time=200]
[reset_camera time=0]
[chara_hide_all]
[bg storage="gym.jpg"]

[mask_off time=100]
[stopbgm]
[chara_mod name="yamato"  face="angry"]
[chara_mod name="akane"  face="back"]
[chara_show name="akane" width="400" height="600" left=20 top=80]
[wait time=100]
[chara_show name="yamato" left=480 top=0]

#キャラ２ 
……おまえ、[r]
いったい何やってるんだ？？[l]

[cm]
[playbgm storage="comedy.ogg"]
[anim name=akane top="-=10" time=100]
[wait time=100]
[anim name=akane top="+=10" time=100]

[chara_mod name="akane"  face="doki"]

#キャラ１ 
キャ、キャラ２！？[l]

[cm]

[chara_mod name="yamato"  face="tohoho"]

#キャラ２ 
あまりにも真剣にやってるから、[r]
ちょっと声かけ辛くてな……[l]

[cm]
[chara_mod name="akane"  face="sad"]

[anim name=akane left="-=10" time=200]
[wait time=200]
[anim name=akane left="+=10" time=200]
[wait time=200]
[anim name=akane left="-=10" time=200]
[wait time=200]
[anim name=akane left="+=10" time=200]
[wa]
#キャラ１ 
あはは……[l]

[anim name=akane top="-=10" time=100]
[wait time=100]
[anim name=akane top="+=10" time=100]
[wa]
[cm]
[chara_mod name="akane"  face="normal"]
#キャラ１ 
実は、演劇の練習をしていたんだ！[l]

[cm]


[chara_mod name="akane"  face="happy"]

[anim name=akane left="+=30" time=200  effect="easeOutCirc"]

#キャラ１ 
あ！　キャラ２も練習手伝ってよ！[r]
これ台本ね！[l]

[cm]

[chara_mod name="yamato"  face="angry"]

[anim name=yamato left="+=10" time=100]

#キャラ２ 
まだやるって言ってないんだが……[l]

[cm]

[chara_mod name="akane"  face="normal"]
[anim name=akane left="+=10" time=100]
#キャラ１ 
まあまあ、そんなこと言わずに……[l]

[cm]


[chara_mod name="akane"  face="angry"]
[anim name=akane left="+=30" time=100]
#キャラ１ 
あとでアイス御馳走するから！[l]

[cm]
[chara_mod name="yamato"  face="happy"]
[anim name=yamato top="+=30" time=200]
[wait time=200]
[anim name=yamato top="-=30" time=200]
[wa]

#キャラ２ 
やらせていただきます！[l]

[cm]

[chara_mod name="akane"  face="happy"]

[anim name=akane top="-=10" time=100]
[wait time=100]
[anim name=akane top="+=10" time=100]
[wa]

#キャラ１ 
本当に！　やった！[l]

[cm]

#キャラ１ 
よーし！[r]
練習再開だ！[l]

[mask]
[chara_hide_all]
[chara_hide_all layer=1]

[freeimage layer="base"]
#
@layopt layer=message0 visible=false
[mask_off]
[jump first.ks]


