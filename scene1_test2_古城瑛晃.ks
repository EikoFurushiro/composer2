*start

[cm]
[clearfix]
[chara_config pos_mode=false]

[mask time="0"]


[position layer="message0" left=20 top=450 width=920 height=150 page=fore visible=true]

[position layer=message0 page=fore margint="35" marginl="50" marginr="70" marginb="60"]

[ptext name="chara_name_area" layer="message0" color="white" size=22 x=50 y=460]

[chara_config ptext="chara_name_area"]

@showmenubutton


[chara_new name="akane" storage="chara/akane/normal.png" jname="あかね"]

[chara_face name="akane" face="normal" storage="chara/akane/normal.png"]
[chara_face name="akane" face="happy" storage="chara/akane/happy.png"]
[chara_face name="akane" face="angry" storage="chara/akane/angry.png"]
[chara_face name="akane" face="sad" storage="chara/akane/sad.png"]
[chara_face name="akane" face="doki" storage="chara/akane/doki.png"]
[chara_face name="akane" face="back" storage="chara/akane/back.png"]

[chara_new name="aoi" storage="chara/aoi/normal.png" jname="あおい"]

[chara_face name="aoi" face="happy" storage="chara/aoi/happy.png"]
[chara_face name="aoi" face="normal" storage="chara/aoi/normal.png"]
[chara_face name="aoi" face="angry" storage="chara/aoi/angry.png"]
[chara_face name="aoi" face="doki" storage="chara/aoi/doki.png"]
[chara_face name="aoi" face="sad" storage="chara/aoi/sad.png"]
;背景
[image layer=0 folder="bgimage" name="aosora" storage="aosora.jpg" visible=true top=0 left=0 width=1200 height=675]
[chara_show  name="aoi" left=280 top=50  time="0"]
[wa]

[mask_off time=1000]
[wait time=200]

[mtext text="ざざーん……" x=230 y=120 size=30 in_effect="fadeIn" in_sync=true time="0" out_effect="fadeOut" out_sync=true wait=false]
[wait time=1000]
[mtext text="ざざーん……" x=620 y=270 size=30 in_effect="fadeIn" in_sync=true time="0" out_effect="fadeOut" out_sync=true wait=false]
[wait time=2000]
@layopt layer=message0 visible=true

#あかね　
あおいちゃん、海まで来て制服って、ないと思うんだ……[p]

[chara_mod  name="aoi" face="angry"]
[anim name=aoi top="-=10" time=100]
[wait time=100]
[anim name=aoi top="+=10" time=100]


#あおい　
なに言ってるの？[r]
学生はどんな時でも制服を着用するものよ！[p]


[chara_mod  name="aoi" face="happy"]
[camera x=0 zoom=1.2 time=300] 
#あおい　
あかねも制服に着替えましょう！[p]

@layopt layer=message0 visible=false

[anim name=aosora left="-=20" effect="easeOutQuint" time=500 wait=false]
[anim name=aoi left="-=20" effect="easeOutQuint" time=500 wait=false]
[wait time=500]
[anim name=aosora left="+=20" effect="easeOutQuint" time=500 wait=false]
[anim name=aoi left="+=20" effect="easeOutQuint" time=500 wait=false]
[wait time=500]

@layopt layer=message0 visible=true

#あかね　
やだよ……、[r]
私は海で泳いてくる！[p]

[chara_mod  name="aoi" face="doki"]
[camera x=100 zoom=1.0 time200 wait=false] 
[wait time=200]
[chara_hide name=aoi" ]


#あかね　
じゃあね、あおいちゃん！[p]

@layopt layer=message0 visible=false

[mask     time="700" effect="fadeInUpBig" ]
[image layer=0 folder="bgimage" name="suizokukan2" storage="suizokukan2.jpg" visible=true  top=0 left=0 width=1200 height=675]
[wait time="700"]
[mask_off time="500" effect="fadeOutUpBig" wait="true"     ]

[wait time="1000"]


[mask time="700" effect="rotateIn" ]

[free layer=0 name="suizokukan2"]
[free layer=0 name="aozora"]

[image layer="0" folder="bgimage" name="living" storage="living.jpg" visible=true  top=0 left=0 width=1200 height=675]
[chara_mod  name="aoi" face="angry"]
[chara_show  name="aoi" left=180 top=50 layer="0" time="0"]
[image layer="0" folder="bgimage" name="kuro2" storage="kuro.jpg" visible="true" top="100" left="0" width="1200" height="675" time="0"]
[image layer="0" folder="bgimage" name="kuro" storage="kuro.jpg" visible="true" top="-500" left="0" width="1200" height="675" time="0"]
[camera x="0" time="0"]
[camera zoom="2" x="-180" y="120" rotate="-25"]

[mask_off time="500"]
@layopt layer=message0 visible=true

[anim  name="kuro2" top="+=200" time="2000"]
[anim  name="kuro"top="-=200" time="2000"]
[wa]

#あおい　
あかね、起きて！[r]
こんなところで寝たら風邪ひくよ！[p]


[quake count=4 time=500 hmax=40]
[wa]
#あかね　
……うぅ……寒い……[r]
――っくしゅん！[p]


[mask  time="200"]

[reset_camera time="0"]
[chara_hide_all]
[freeimage layer="0"]

[image layer="0" folder="bgimage" name="living" storage="living.jpg" visible=true  top=0 left=0 width=1200 height=675]
[chara_mod  name="aoi" face="normal"]
[chara_show  name="aoi" left=70 top=50 layer="0" time="0"]

[chara_mod  name="akane" face="sad"]
[chara_show  name="akane" left=500 top=50 layer="0" time="0"]

[mask_off time="500"]


[chara_mod  name="aoi" face="sad"]
[anim name=aoi top="-=10" time=100]
[wait time=100]
[anim name=aoi top="+=10" time=100]
#あおい　
はぁ……、[r]
風邪ひいたんじゃない？[p]

[chara_mod  name="aoi" face="normal"]

#あおい　
ちょっとこっちに来て、[r]
おでこ出してみて[p]

[chara_mod  name="akane" face="doki"]

[anim name=akane left="-=10" time=100]
[wait time=100]
[anim name=akane left="+=10" time=100]

#あかね　
え？[r]
……あれ……私の海は？[p]

[chara_mod  name="aoi" face="angry"]

[anim name=aoi top="-=10" time=100]
[wait time=100]
[anim name=aoi top="+=10" time=100]
[wait time=100]
[anim name=aoi top="-=10" time=100]
[wait time=100]
[anim name=aoi top="+=10" time=100]
#あおい　
なに訳のわからないこと言ってるの！[r]
いいからおでこを出す！[p]

[chara_hide_all time=200]

[image layer="0" folder="bgimage" name="living" storage="living.jpg" visible="true" top="-50" left="0" width="1200" height="675"]
[image layer="1" folder="bgimage" name="kuro" storage="kuro.jpg" visible="true" top="450" left="0" time="100" wait=false]
[image layer="1" folder="bgimage" name="kuro" storage="kuro.jpg" visible="true" top="-1020" left="0" time="100" wait=true]

[chara_show name="aoi" face="sad" left=-80 top=-50 width="800" height="1200" wait=false]
[chara_show name="akane" face="back" left=250 top=-50 width="800" height="1200" wait=true]

[anim name=akane left="-=10" time=100]
[wait time=100]
[anim name=akane left="+=10" time=100]

#あかね　
あ……[p]

[anim name=akane left="-=10" time=100]
[wait time=100]
[anim name=akane left="+=10" time=100]
[wait time=100]
[anim name=akane left="-=10" time=100]
[wait time=100]
[anim name=akane left="+=10" time=100]
#あかね　
……えっと、それは恥ずかしいよ～[p]

[chara_mod  name="aoi" face="normal"]

[anim name=aoi left="+=10" time=100]
[anim name=akane left="+=5" time=100]
#あおい　
いいから！[r]
うーん、ちょっと熱っぽい気もするけど……[p]

[anim name=aoi left="+=30" time=100]
[anim name=akane left="+=10" time=100]
#あおい　
もう少ししっかりおでこかしなさいよ[p]

[chara_mod  name="aoi" face="sad"]
[anim name=aoi left="-=5" time=100]
[wait time=100]
[anim name=aoi left="+=5" time=100]

#あおい　
うーん、やっぱり少しありそうな感じね……[p]

[mask  time="200"]
[reset_camera time="0"]
[freeimage layer="0"]
[freeimage layer="1"]
[image layer="0" folder="bgimage" name="living" storage="living.jpg" visible=true  top=0 left=0 width=1200 height=675]
[chara_mod  name="aoi" face="normal"]
[chara_show  name="aoi" left=170 top=50 width="400" height="600" layer="1" time="0"]

[chara_mod  name="akane" face="doki"]
[chara_show  name="akane" left=450 top=50   width="400" height="600" layer="1" time="0"]

[mask_off time="500"]

[anim name=akane left="+=10" time=100]
[wait time=100]
[anim name=akane left="+=20" time=100]

#あかね　
だ、大丈夫だから！[r]　
そろそろ、お風呂入って寝るから！[p]

[chara_mod  name="akane" face="happy"]

#あかね　
あおいちゃんも風邪ひかないように気を付けてね！[p]

[mask time=3000]
[reset_camera time="0"]
[chara_hide_all]
[freeimage layer="0"]
[freeimage layer="1"]
[mask_off]
[jump first.ks]
